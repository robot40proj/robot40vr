﻿using System;
using UnityEngine;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.MessageTypes.Std;

[RequireComponent(typeof(RosConnector))]
public class ToolController : MonoBehaviour
{
    public string Service = "";
    public OVRInput.RawAxis1D toolControllTrigger = OVRInput.RawAxis1D.RHandTrigger;
    public float TimeStep = 0.2f;

    private float lastValue = -1.0f;
    private RosConnector rosConnector;
    private DateTime lastSend = DateTime.Now;
    private bool canMove = true;

    void Start()
    {
        rosConnector = GetComponent<RosConnector>();
    }

    private void GripperCallHandler(Bool message)
    {
        Debug.Log("Success: " + message.data);
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if ((float)((DateTime.Now - this.lastSend).Milliseconds) / 1000.0f >= this.TimeStep && canMove)
            {
                
                var value = OVRInput.Get(toolControllTrigger);
                if (value != lastValue) {
                    this.lastSend = DateTime.Now;
                    lastValue = value;
                    canMove = false;
                    this.rosConnector.RosSocket.CallService<RG6GripRequest, Bool>(Service, GripperCallHandler, new RG6GripRequest(160.0 - (value * 160.0)));
                }
            }
    }
}
