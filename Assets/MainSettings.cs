﻿using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Protocols;
using UnityEngine;

public class MainSettings : MonoBehaviour
{
    [System.NonSerialized]
    public string RosBridgeServerUrl;
    public RosSocket.SerializerEnum Serializer;
    public Protocol protocol;
    public ROSFinder RosFinder;
    public int SecondsTimeout = 10;

    public MainSettings()
    {
    }
    public virtual void Awake()
    {
        this.RosBridgeServerUrl = this.RosFinder.GetRosURL();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.RosBridgeServerUrl is null)
        {
            this.RosBridgeServerUrl = this.RosFinder.GetRosURL();
        }
    }
}
