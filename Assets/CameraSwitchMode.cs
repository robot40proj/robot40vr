﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchMode : MonoBehaviour
{
    
    public GameObject PlayerController;
    public GameObject Head;
    
    //state stores camera mode: 0 is for TPP, 1 is for FPP
    private int state=0;
    private bool changing=false;
    void Start()
    {
        if (this.transform.IsChildOf(PlayerController.transform)){
            state = 0;
        }
        else if (this.transform.IsChildOf(Head.transform)){
            state = 1;
        }
        else{
            state = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        if (OVRInput.GetDown(OVRInput.Button.Four)){
            if(!changing){
                switch(state){
                    case 0:
                        this.transform.SetParent(Head.transform, false);
                        state = 1;
                        break;
                    case 1:
                        this.transform.SetParent(PlayerController.transform, false);
                        state = 0;
                        break;
                    default:
                        break;
                }
            }
        }
        else {
            if(OVRInput.GetUp(OVRInput.Button.Four) && changing) changing = false;
        }
    }
}
