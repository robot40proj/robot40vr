﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RosSharp.RosBridgeClient;

public class CmdJoyPublisher : MonoBehaviour
{
    public string Topic = "";
    public OVRInput.RawAxis2D baseMoveStick = OVRInput.RawAxis2D.LThumbstick;
    public OVRInput.RawAxis1D baseMoveEnableTrigger = OVRInput.RawAxis1D.LHandTrigger;
    public float TimeStep;
    public OVRPlayerController playerController;

    private RosConnector rosConnector;
    private string advertiser;
    private DateTime lastSend = DateTime.Now;

    void Start()
    {
        this.rosConnector = GetComponent<RosConnector>();
        this.advertiser = rosConnector.RosSocket.Advertise<RosSharp.RosBridgeClient.MessageTypes.Geometry.Twist>(Topic);
    }

    // Update is called once per frame
    void Update()
    {
        bool moveEnabled = OVRInput.Get(baseMoveEnableTrigger) > 0.5f;
        if (moveEnabled)
        {
            playerController.EnableLinearMovement = false;
        }
        else
        {
            playerController.EnableLinearMovement = true;
        }
        if ((float)((DateTime.Now - this.lastSend).TotalMilliseconds) / 1000.0f >= this.TimeStep)
        {
            var pub = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Twist();
            if (!moveEnabled)
            {
                this.lastSend = DateTime.Now;
                rosConnector.RosSocket.Publish(this.advertiser, pub);
            }
            else
            {
                var state = OVRInput.Get(baseMoveStick);
                this.lastSend = DateTime.Now;
                pub.linear = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Vector3(state.y * 0.4, 0, 0);
                pub.angular = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Vector3(0, 0, -state.x * 0.2);
                rosConnector.RosSocket.Publish(this.advertiser, pub);
            }
        }
    }
}
