﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChangedEventArgs : EventArgs
{
    public float Value { get; }
    public TriggerChangedEventArgs(float _value)
    {
        Value = _value;
    }
}

public class StickChangedEventArgs : EventArgs
{
    public Vector2 Value { get; }
    public StickChangedEventArgs(Vector2 _value)
    {
        Value = _value;
    }
}

public class ButtonEventDispatcher
{
    public event EventHandler OnReleased;
    public bool prevState = false;
    public OVRInput.RawButton button;

    public ButtonEventDispatcher(OVRInput.RawButton _button)
    {
        this.button = _button;
    }
    public void DispatchReleased()
    {
        this.OnReleased(this, null);
    }
}

public class TriggerEventDispatcher
{
    public event EventHandler OnChanged;
    public float prevValue = 0.0f;
    public OVRInput.RawAxis1D trigger;

    public TriggerEventDispatcher(OVRInput.RawAxis1D _trigger)
    {
        this.trigger = _trigger;
    }
    public void DispatchReleased()
    {
        this.OnChanged(this, new TriggerChangedEventArgs(prevValue));
    }
}

public class StickEventDispatcher
{
    public event EventHandler OnChanged;
    public Vector2 prevValue = new Vector2();
    public OVRInput.RawAxis2D stick;

    public StickEventDispatcher(OVRInput.RawAxis2D _stick)
    {
        this.stick = _stick;
    }
    public void DispatchReleased()
    {
        this.OnChanged(this, new StickChangedEventArgs(prevValue));
    }
}

public class ControllerEventHandler : MonoBehaviour
{
    private Dictionary<OVRInput.RawButton, ButtonEventDispatcher> ControllerButtonReleasedEvents = new Dictionary<OVRInput.RawButton, ButtonEventDispatcher>();
    private Dictionary<OVRInput.RawAxis1D, TriggerEventDispatcher> ControllerTriggerEvents = new Dictionary<OVRInput.RawAxis1D, TriggerEventDispatcher>();
    private Dictionary<OVRInput.RawAxis2D, StickEventDispatcher> ControllerStickEvents = new Dictionary<OVRInput.RawAxis2D, StickEventDispatcher>();

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        foreach (var kvp in this.ControllerButtonReleasedEvents)
        {
            bool pressed = OVRInput.Get(kvp.Key);
            if (!pressed)
            {
                if (kvp.Value.prevState)
                {
                    kvp.Value.DispatchReleased();
                }
            }
            kvp.Value.prevState = pressed;
        }
        foreach (var ev in ControllerTriggerEvents)
        {
            float value = OVRInput.Get(ev.Key);
            if (value != ev.Value.prevValue)
            {
                ev.Value.DispatchReleased();
            }
            ev.Value.prevValue = value;
        }
        foreach (var sev in ControllerStickEvents)
        {
            Vector2 value = OVRInput.Get(sev.Key);
            if (value.x != sev.Value.prevValue.x || value.y != sev.Value.prevValue.y)
            {
                sev.Value.DispatchReleased();
            }
            sev.Value.prevValue = value;
        }
    }
    public ButtonEventDispatcher GetDispatcher(OVRInput.RawButton button)
    {
        if (!this.ControllerButtonReleasedEvents.ContainsKey(button))
        {
            this.ControllerButtonReleasedEvents.Add(button, new ButtonEventDispatcher(button));
        }
        return this.ControllerButtonReleasedEvents[button];
    }

    public TriggerEventDispatcher GetDispatcher(OVRInput.RawAxis1D triger)
    {
        if (!this.ControllerTriggerEvents.ContainsKey(triger))
        {
            this.ControllerTriggerEvents.Add(triger, new TriggerEventDispatcher(triger));
        }
        return this.ControllerTriggerEvents[triger];
    }

    public StickEventDispatcher GetDispatcher(OVRInput.RawAxis2D stick)
    {
        if (!this.ControllerStickEvents.ContainsKey(stick))
        {
            this.ControllerStickEvents.Add(stick, new StickEventDispatcher(stick));
        }
        return this.ControllerStickEvents[stick];
    }
}
