﻿using Ionic.Zlib;
using RosSharp.RosBridgeClient;
using System;
using System.Threading;
using UnityEngine;

public class OccupancyGridSubscriber : UnitySubscriber<RosSharp.RosBridgeClient.MessageTypes.Compressor.CompressedMap>
{
    public MapRenderer mapRenderer;
    public Transform mapPlaneTransform;
    public float MapUpdateTimestep = 1.0f;

    private object lockObj = new object();
    private sbyte[] prevMap;

    private DateTime lastMapTextureUpdate = DateTime.Now;
    private RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose mapOrigin = new RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose();

    private bool doneRendering = true;
    protected override void Start()
    {
        base.Start();
    }


    void Update()
    {
        if (((float)((DateTime.Now - this.lastMapTextureUpdate).TotalMilliseconds) / 1000.0f) > this.MapUpdateTimestep)
        {
            this.lastMapTextureUpdate = DateTime.Now;
            lock (lockObj)
            {
                if (mapRenderer.Origin != null)
                    mapRenderer.updateMap();
            }
        }
    }
    private void ParseMessage(RosSharp.RosBridgeClient.MessageTypes.Compressor.CompressedMap message)
    {
        sbyte[] bytes = (sbyte[])(Array)(ZlibStream.UncompressBuffer(Convert.FromBase64String(message.data)));
        lock (lockObj)
        {
            mapRenderer.Resolution = message.info.resolution;
            mapRenderer.Origin = message.info.origin;
            mapRenderer.MapWidth = message.info.width;
            mapRenderer.MapHeight = message.info.height;
            mapOrigin = message.info.origin;
            prevMap = bytes;
            mapRenderer.GenerateMap(prevMap);
            this.doneRendering = true;
        }
    }
    protected override void ReceiveMessage(RosSharp.RosBridgeClient.MessageTypes.Compressor.CompressedMap message)
    {
        lock (lockObj)
        {
            if (this.doneRendering)
            {
                this.doneRendering = false;
                ThreadPool.QueueUserWorkItem(o => this.ParseMessage(message));
            }
        }
    }
}
