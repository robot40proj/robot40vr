﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    public Vector3 Position;
    // Start is called before the first frame update
    void Start()
    {
        OVRInput.Update();
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
    }

    void Move(Vector3 position){
        this.transform.position = position;
    }
}
