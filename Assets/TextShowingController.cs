﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextShowingController : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<Text>().enabled = false;
    }

    void Update()
    {
        this.GetComponent<Text>().enabled = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.5f ? true : false;
    }
}
