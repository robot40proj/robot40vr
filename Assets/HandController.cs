﻿using RosSharp.RosBridgeClient;
using System;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public ControllerEventHandler EventHandler;
    public string Frame_id = "base_link";
    public string Topic = "";
    public string OrderTopic = "";
    public OVRInput.RawButton sendArmMoveButton = OVRInput.RawButton.A;
    public OVRInput.RawButton sendMoveInitButton = OVRInput.RawButton.B;
    public GameObject HandObject;
    public GameObject BaseLinkObject;

    private RosConnector rosConnector;
    private string advertiser;
    private string order_advertiser;

    // Start is called before the first frame update
    void Start()
    {
        this.rosConnector = GetComponent<RosConnector>();
        this.advertiser = rosConnector.RosSocket.Advertise<RosSharp.RosBridgeClient.MessageTypes.Geometry.PoseStamped>(Topic);
        this.order_advertiser = rosConnector.RosSocket.Advertise<RosSharp.RosBridgeClient.MessageTypes.Std.String>(OrderTopic);
        EventHandler.GetDispatcher(this.sendArmMoveButton).OnReleased += (object sender, EventArgs e) =>
        {
            var baseLinkOrigin = BaseLinkObject.transform;
            Vector3 baseLinkRelative = baseLinkOrigin.InverseTransformPoint(this.HandObject.transform.position);
            Quaternion rotationDelta = Quaternion.Inverse(BaseLinkObject.transform.rotation) * this.HandObject.transform.rotation; //Quaternion.FromToRotation(baseLinkOrigin.transform.forward, this.HandObject.transform.forward);
            var pose = new RosSharp.RosBridgeClient.MessageTypes.Geometry.PoseStamped();
            pose.header.frame_id = Frame_id;
            //pose.header.stamp = ?
            //For World Space
            /*
            var rosVec3 = RosSharp.TransformExtensions.Unity2Ros(this.HandObject.transform.position);
            var targetRotation = Quaternion.AngleAxis(180.0f, this.HandObject.transform.forward) * this.HandObject.transform.rotation; //rotate Z axis by 180 degrees
            var rosQuat = RosSharp.TransformExtensions.Unity2Ros(targetRotation);
            */
            //for relative space
            var rosVec3 = RosSharp.TransformExtensions.Unity2Ros(baseLinkRelative);
            var targetRotation = rotationDelta * Quaternion.Euler(Vector3.forward * 180); ; //Quaternion.AngleAxis(180.0f, baseLinkRelative.forward) * rotationDelta; //rotate Z axis by 180 degrees
            var rosQuat = RosSharp.TransformExtensions.Unity2Ros(targetRotation);
            pose.pose.position.x = rosVec3.x;
            pose.pose.position.y = rosVec3.y;
            pose.pose.position.z = rosVec3.z;
            pose.pose.orientation.x = rosQuat.x;
            pose.pose.orientation.y = rosQuat.y;
            pose.pose.orientation.z = rosQuat.z;
            pose.pose.orientation.w = rosQuat.w;
            rosConnector.RosSocket.Publish(this.advertiser, pose);
        };
        EventHandler.GetDispatcher(this.sendMoveInitButton).OnReleased += (object sender, EventArgs e) =>
        {
            var order = new RosSharp.RosBridgeClient.MessageTypes.Std.String("moveInit");
            rosConnector.RosSocket.Publish(this.order_advertiser, order);
        };
    }

    // Update is called once per frame
    void Update()
    {

    }
}
