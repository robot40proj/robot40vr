﻿using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.MessageTypes.Nav;

using RosSharp.RosBridgeClient.MessageTypes.Std;
namespace RosSharp.RosBridgeClient.MessageTypes.Compressor
{
    public class CompressedMap : Message
    {
        public const string RosMessageName = "map_compressor/CompressedMap";

        public Header header { get; set; }
        public MapMetaData info { get; set; }
        public string data { get; set; }

        public CompressedMap()
        {
            this.header = new Header();
            this.info = new MapMetaData();
            this.data = "";
        }

        public CompressedMap(Header header, MapMetaData info, string data)
        {
            this.header = header;
            this.info = info;
            this.data = data;
        }
    }
}