﻿using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.MessageTypes.Nav;
using RosSharp.RosBridgeClient.MessageTypes.Std;

public class RG6GripRequest : Message
{
    public const string RosMessageName = "rg6_service/RG6_gripRequest";
    public Float64 target_width { get; set; }

    public RG6GripRequest()
    {
        this.target_width = new Float64(160.0);
    }

    public RG6GripRequest(double width)
    {
        this.target_width = new Float64(width);
    }
    public RG6GripRequest(Float64 width)
    {
        this.target_width = width;
    }
}
