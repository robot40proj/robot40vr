﻿using System;
using UnityEngine;

public class MapRenderer : MonoBehaviour
{
    //public uint width { private get; set; }
    //public uint height { private get; set; }
    public Transform MapSurface;
    public float renderDistance;
    public Transform RobotTransform;
    public float Resolution { private get; set; }
    [NonSerialized]
    public RosSharp.RosBridgeClient.MessageTypes.Geometry.Pose Origin;
    public uint MapWidth { private get; set; }
    public uint MapHeight { private get; set; }

    private MeshRenderer meshRenderer;
    private float planeMeshSize = 10.0f;

    private object lockObject = new object();
    private Color[] colors;

    // Start is called before the first frame update
    void Start()
    {
        Texture.allowThreadedTextureCreation = true;
        meshRenderer = GetComponent<MeshRenderer>();
        Vector3 plainSize = new Vector3(renderDistance / planeMeshSize, 1, renderDistance / planeMeshSize);
        MapSurface.localScale = plainSize;
    }
    private void createTexture(sbyte[] map, int rowSize)
    {
        Color[] colors = new Color[rowSize * rowSize];
        int i = 0;
        for (int y = 0; y < rowSize; y++)
        {
            for (int x = 0; x < rowSize; x++)
            {
                int val = 255 - (map[y * rowSize + x] / 100 * 255);
                Color pixelColor = new Color(val, val, val);
                colors[i] = pixelColor;
                i = i + 1;
            }
        }
        lock (lockObject)
        {
            this.colors = colors;
        }
    }

    private Tuple<sbyte[], int> cropMap(sbyte[] map)
    {
        int size = (int)(renderDistance / Resolution);
        int xOffset = Math.Abs((int)(Origin.position.x / Resolution)) - size / 2;
        int yOffset = Math.Abs((int)(Origin.position.y / Resolution)) - size / 2;
        sbyte[] map_cropped = new sbyte[size * size];
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                map_cropped[(y * size) + x] = (sbyte)(map[((y + yOffset) * 4000) + x + xOffset] - 127);
            }
        }

        return new Tuple<sbyte[], int>(map_cropped, size);
    }
    public void GenerateMap(sbyte[] map)
    {
        var croppedMap = cropMap(map);
        createTexture(croppedMap.Item1, croppedMap.Item2);
    }
    public void updateMap()
    {
        if (this.colors == null) return;
        int size = (int)(renderDistance / Resolution);
        lock (lockObject)
        {
            if (meshRenderer.material.mainTexture != null)
                Destroy(meshRenderer.material.mainTexture);
            var texture = new Texture2D(size, size);
            texture.SetPixels(0, 0, size, size, colors);
            meshRenderer.material.mainTexture = texture;
            texture.Apply();
        }
    }

    void Update()
    {

    }
}
