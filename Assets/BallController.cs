﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float speed;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        OVRInput.Update();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        float moveHorizontal = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick)[0];
        float moveVertical = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick)[1];

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed * Time.deltaTime);
    }
}
