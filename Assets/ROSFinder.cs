﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ROSFinder : MonoBehaviour
{
    private UdpClient client;
    public int Port = 21337;
    private int rosBridgePort = 9090;
    private string rosBridgeAddr = "127.0.0.1";
    // Start is called before the first frame update

    public ROSFinder()
    {

    }
    public virtual void Awake()
    {
        var t = Task.Factory.StartNew(() => FindROS());
        t.Wait();
        Debug.Log("Rosbridge url discovered: " + this.GetRosURL());
    }
    void Start()
    {

    }
    public string GetRosURL()
    {
        return "ws://" + this.rosBridgeAddr + ":" + this.rosBridgePort;
    }
    void FindROS()
    {
        List<Task> tasks = new List<Task>();
        var timeout = Task.Delay(5000);
        tasks.Add(timeout);
        NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();
        foreach (NetworkInterface Interface in Interfaces)
        {
            UnicastIPAddressInformationCollection UnicastIPInfoCol = Interface.GetIPProperties().UnicastAddresses;
            foreach (UnicastIPAddressInformation UnicatIPInfo in UnicastIPInfoCol)
            {
                if (UnicatIPInfo.Address.AddressFamily != AddressFamily.InterNetwork) continue;
                if (!UnicatIPInfo.Address.ToString().StartsWith("127."))
                {
                    var subnetBroadast = new IPEndPoint(((uint)UnicatIPInfo.Address.Address) | (~((uint)UnicatIPInfo.IPv4Mask.Address)), this.Port);
                    var task = askForIP(subnetBroadast);
                    tasks.Add(task);
                }
            }
        }
        int index = Task.WaitAny(tasks.ToArray());
        if (index != 0)
        {
            var task = tasks[index] as Task<IPEndPoint>;
            IPEndPoint ep = task.Result;
            this.rosBridgeAddr = ep.Address.ToString();
            this.rosBridgePort = ep.Port;
        }
        else
        {
            Debug.LogError("Timeout while waiting for ROS ip addr");
        }
    }
    async Task<IPEndPoint> askForIP(IPEndPoint endpoint)
    {
        var Client = new UdpClient();
        var RequestData = Encoding.ASCII.GetBytes("ROS");
        var ServerEp = new IPEndPoint(IPAddress.Any, 0);
        Client.EnableBroadcast = true;
        Client.Send(RequestData, RequestData.Length, endpoint);
        var ServerResponseData = await Client.ReceiveAsync();
        ServerEp = ServerResponseData.RemoteEndPoint;
        var ServerResponse = Encoding.ASCII.GetString(ServerResponseData.Buffer);
        Client.Close();
        byte[] portBytesBigendian = ServerResponseData.Buffer.Skip(3).Take(2).ToArray();
        int port = (portBytesBigendian[0] << 8) + portBytesBigendian[1];
        return new IPEndPoint(ServerEp.Address, port);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
