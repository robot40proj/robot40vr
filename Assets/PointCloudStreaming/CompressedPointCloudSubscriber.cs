﻿using Ionic.Zlib;
using System;
using System.Threading;
using UnityEngine;


namespace RosSharp.RosBridgeClient
{
    [RequireComponent(typeof(RosConnector))]
    public class CompressedPointCloudSubscriber : UnitySubscriber<MessageTypes.Compressor.CompressedPointcloud2>
    {
        public Transform offset; // Put any gameobject that faciliatates adjusting the origin of the pointcloud in VR. 
        private byte[] byteArray;
        private int size;

        private Vector3[] pcl;
        private Color[] pcl_color;
        private int[] pcl_indices;

        int width;
        int height;
        int row_step;
        int point_step;

        private object lockObject = new object();
        [NonSerialized]
        public DateTime lastRendered = DateTime.Now;
        private bool doneRendering = true;


        protected override void Start()
        {
            base.Start();

        }
        private bool updatePositions = false;
        public void Update()
        {
            if (updatePositions)
            {
                updatePositions = false;
                offsetPosition = new Vector3(offset.position.x, offset.position.y, offset.position.z);
                offsetRotation = new Quaternion(offset.rotation.x, offset.rotation.y, offset.rotation.z, offset.rotation.w);
            }
        }

        private byte[] uncompress(string data)
        {
            return (byte[])(Array)(ZlibStream.UncompressBuffer(Convert.FromBase64String(data)));
        }
        private void ParseMessage(MessageTypes.Compressor.CompressedPointcloud2 message)
        {
            byte[] messageData = uncompress(message.data);
            size = messageData.GetLength(0);
            byteArray = new byte[size];
            byteArray = messageData;


            width = (int)message.width;
            height = (int)message.height;
            row_step = (int)message.row_step;
            point_step = (int)message.point_step;

            size = size / point_step;

            PointCloudRendering();
        }
        protected override void ReceiveMessage(MessageTypes.Compressor.CompressedPointcloud2 message)
        {
            lock (lockObject)
            {
                if (this.doneRendering)
                {
                    this.doneRendering = false;
                    this.updatePositions = true;
                    ThreadPool.QueueUserWorkItem(o => this.ParseMessage(message));
                }
            }
        }

        void PointCloudRendering()
        {
            var pcl = new Vector3[size];
            var pcl_color = new Color[size];
            int[] indices = new int[size];

            int x_posi;
            int y_posi;
            int z_posi;

            float x;
            float y;
            float z;

            int rgb_posi;
            int rgb_max = 255;

            float r;
            float g;
            float b;
            for (int n = 0; n < size; n++)
            {
                x_posi = n * point_step + 0;
                y_posi = n * point_step + 4;
                z_posi = n * point_step + 8;

                x = BitConverter.ToSingle(byteArray, x_posi);
                y = BitConverter.ToSingle(byteArray, y_posi);
                z = BitConverter.ToSingle(byteArray, z_posi);


                rgb_posi = n * point_step + 16;

                b = byteArray[rgb_posi + 0];
                g = byteArray[rgb_posi + 1];
                r = byteArray[rgb_posi + 2];

                r = r / rgb_max;
                g = g / rgb_max;
                b = b / rgb_max;

                pcl[n] = new Vector3(x, z, y);
                pcl_color[n] = new Color(r, g, b);
                indices[n] = n;
            }
            lock (lockObject)
            {
                this.pcl = pcl;
                this.pcl_color = pcl_color;
                this.pcl_indices = indices;
                lastRendered = DateTime.Now;
                this.doneRendering = true;
            }
        }

        public Vector3[] GetPCL()
        {
            lock (lockObject)
            {
                return pcl;
            }
        }

        public Color[] GetPCLColor()
        {
            lock (lockObject)
            {
                return pcl_color;
            }
        }
        public int[] GetIndices()
        {
            lock (lockObject)
            {
                return pcl_indices;
            }
        }
        private Vector3 offsetPosition;
        public Vector3 GetOffsetPosition()
        {
            lock (lockObject)
            {
                return offsetPosition;
            }
        }
        private Quaternion offsetRotation;
        public Quaternion GetOffsetRotation()
        {
            lock (lockObject)
            {
                return offsetRotation;
            }
        }
    }
}
