﻿using RosSharp.RosBridgeClient.MessageTypes.Std;

namespace RosSharp.RosBridgeClient.MessageTypes.Compressor
{
    public class CompressedPointcloud2 : Message
    {
        public const string RosMessageName = "map_compressor/CompressedPointcloud2";

        //  This message holds a collection of N-dimensional points, which may
        //  contain additional information such as normals, intensity, etc. The
        //  point data is stored as a binary blob, its layout described by the
        //  contents of the "fields" array.
        //  The point cloud data may be organized 2d (image-like) or 1d
        //  (unordered). Point clouds organized as 2d images may be produced by
        //  camera depth sensors such as stereo or time-of-flight.
        //  Time of sensor data acquisition, and the coordinate frame ID (for 3d
        //  points).
        public Header header { get; set; }
        //  2D structure of the point cloud. If the cloud is unordered, height is
        //  1 and width is the length of the point cloud.
        public uint height { get; set; }
        public uint width { get; set; }
        //  Describes the channels and their layout in the binary data blob.
        public string[] fields { get; set; }
        public bool is_bigendian { get; set; }
        //  Is this data bigendian?
        public uint point_step { get; set; }
        //  Length of a point in bytes
        public uint row_step { get; set; }
        //  Length of a row in bytes
        public string data { get; set; }
        //  Actual point data compressed using zlib, size is (row_step*height)
        public bool is_dense { get; set; }
        //  True if there are no invalid points

        public CompressedPointcloud2()
        {
            this.header = new Header();
            this.height = 0;
            this.width = 0;
            this.fields = new string[0];
            this.is_bigendian = false;
            this.point_step = 0;
            this.row_step = 0;
            this.data = "";
            this.is_dense = false;
        }

        public CompressedPointcloud2(Header header, uint height, uint width, string[] fields, bool is_bigendian, uint point_step, uint row_step, string data, bool is_dense)
        {
            this.header = header;
            this.height = height;
            this.width = width;
            this.fields = fields;
            this.is_bigendian = is_bigendian;
            this.point_step = point_step;
            this.row_step = row_step;
            this.data = data;
            this.is_dense = is_dense;
        }
    }
}