﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMoveController : MonoBehaviour
{
    private float movementSpeed = 0.3f;
    public Transform camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");

        camera.position += new Vector3(horizontalMovement * movementSpeed,0,verticalMovement * movementSpeed);
    }
}
