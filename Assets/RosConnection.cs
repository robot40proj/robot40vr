﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Protocols;

public class RosConnection : MonoBehaviour
{
    public Text messageText;
    void Start()
    {
        
    }

    
    void Update()
    {
        messageText.text = "Is connected? " + GetComponent<RosConnector>().RosSocket.protocol.IsAlive() + 
        ". Value: " + GetComponent<JointStateSubscriber>().Data; 
    }
}
