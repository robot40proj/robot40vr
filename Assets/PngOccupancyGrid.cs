﻿using RosSharp.RosBridgeClient;

public class PngOccupancyGrid : Message
{
    public const string RosMessageName = "nav_msgs/OccupancyGrid";

    public string data { get; set; }

    public PngOccupancyGrid()
    {
        this.data = "";
    }

    public PngOccupancyGrid(string data)
    {
        this.data = data;
    }
}
