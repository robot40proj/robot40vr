﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static OVRHand;

public class HandInteractionsController : MonoBehaviour
{

    public TextMesh messageText;
    // Start is called before the first frame update
    void Start()
    {
        messageText.text = "Skrypt odpalił";
    }

    // Update is called once per frame
    void Update()
    {
        var hand = GetComponent<OVRHand>();
        bool isIndexFingerPinching = hand.GetFingerIsPinching(HandFinger.Index);
        float ringFingerPinchStrength = hand.GetFingerPinchStrength(HandFinger.Ring);
        if(isIndexFingerPinching){
            messageText.text = "Index finger is pinching at strenght: "+ringFingerPinchStrength;
        }
        else{
            messageText.text = "Index finger is not pinching";
        }
    }
}
